<?php

namespace App\Repositories;

class LoanApplicationRepository extends Repository
{

    public static $model = 'LoanApplication';

    protected array $relations = [];

    public function getRepaymentAmount($amount, $tenure, $rate){
        /*Business Logic to set repayment amount*/
        return ($amount *$tenure * $rate)/100;
    }

    public function getSanctionedAmount($amount){
        /*Business Logic to set repayment amount*/
        return $amount * 0.9;
    }

    public function getInterestRate(){
        /*Business Logic to set interest rate*/
        return 10;
    }

    public function getTotalPaymentCycles($totalAmount, $repaymentAmount){
        /*Business Logic to set interest rate*/
        return ceil($totalAmount/$repaymentAmount);
    }

    public function getLateCharges($amount){
        /*Business Logic to set interest rate*/
        return ceil($amount * 0.05);
    }
}
