<?php namespace App\Repositories;

use App\Http\Requests\BaseRequest;
use App\Interfaces\RepositoryInterface;
use App\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Predis\Client;

abstract class Repository implements RepositoryInterface
{

    protected ?BaseRequest $request;
    protected $db;
    protected $mock;
    protected array $mutators = [];
    public array $data;
    public $selected;
    public static array $with = [];
    public static array $appends = [];
    protected array $allowed_models = [];
    protected array $relations = [];
    /**
     * Default Constructor
     * @param BaseRequest $request Include HTTP Requests
     */
    public function __construct(BaseRequest $request=null, Client $redis= null)
    {
        /** Instantiate */
        $this->request = $request;
        $this->redis= $redis;
        if (is_null($this->relations) || empty($this->relations))
            $this->relations = static::$with;            // Appended relations | []

        $this->attributes = static::$appends;        // Appended attributes | []

        /** Stub/Model Mock */
        $model = $this->model(true);
        if (class_exists($model)) {
            $this->mock = new $model;
        }

        /** Mutators */
        if (method_exists($this, 'registerMutators')) {
            try {
                $this->registerMutators();
            } catch (\Exception $e) {

            }
        }
    }

    /**
     * @return mixed
     */
    public function getMock()
    {

        return $this->mock;
    }

    /**
     * Get the Model Name
     * @param boolean $full Full Model Contract URL?
     * @return string
     */
    public function model($full = false)
    {
        return (!$full)
            ? static::$model
            : 'App\\Models\\' . static::$model;
    }

    /**
     * Get the Model Table
     * @return string
     */
    public function table()
    {
        return $this->mock->getTable();
    }

    /**
     * Get Primary Object/Stat Key
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->mock->getKeyName();
    }

    /**
     * Get Primary Object/Stat Key
     * @return string
     */
    public function getPrimaryObject()
    {
        return strtolower($this->model(false));
    }

    /**
     * Get the Model Fillable Attributes
     * @return array
     */
    public function attributes()
    {
        return $this->mock->getFillable();
    }


    /**
     * Get All Resources
     * @return array
     */
    public function all()
    {
        $self = $this;
        $request = $self->request;
        $this->selected = call_user_func_array([$this->model(true), 'with'], [$this->relations]);
        /*TODO : change this.*/
        $this->selected = $this->selected->get()->sortBy($this->request->input('sort'), $this->request->input('sortDirection'));


        return $this->selected;
    }

    /*
     * get all resources with conditions
     * @return array
     * */

    public function findWhere(array $conditions)
    {
        //$request = $self->request;
        if ($this->selected == null)
            $this->selected = call_user_func_array([$this->model(true), 'where'], [$conditions]);
        else
            $this->selected = $this->selected->where($conditions);

        $sql = $this->selected->toSql();
        return $this->selected;
    }

    /**
     * Create a Resource
     * @param array $datapouser_pass
     * @param Illuminate\Database\Eloquent
     * @return Model
     */
    public function create(array $data = [], Model $model = null)
    {
        if (in_array($this->model(true), $this->allowed_models)) {
            $this->selected = call_user_func_array([$this->model(true), 'create'], [
                /** String Data */
                array_filter(array_only($this->getData(), $this->mock->keys), function ($obj) {
                    return (is_scalar($obj));
                })
            ]);
        } else {
            $this->selected = call_user_func_array([$this->model(true), 'firstOrCreate'], [
                /** String Data */
                array_filter(array_only($this->getData(), $this->mock->keys), function ($obj) {
                    return (is_scalar($obj));
                })
            ]);
        }

        return $this->selected;
    }



    /**
     * Delete Resource(s)
     * @param mixed $id
     * @return Model
     */
    public function delete($id, $column = null)
    {
        $this->getModelsById($id, $column)->delete();
    }

    /**
     * Update Resource(s)
     * @param mixed $id
     * @param array $data
     * @return Model
     */
    public function update($id, $data,$column=null)
    {
        $self = $this;
        $self->data = $data;
        $this->selected = $this->getModelsById($id,$column);
        $this->selected->each(function ($Model) use ($self) {

            $Model->update($self->getData());
        });

        /** Call Relations? */
        if (is_callable([$this, 'addRelations'])) {
            $this->addRelations($this->selected);
        }
        return $this->selected;
    }

    /**
     * Get Model(s) by ID or ID Array
     * @param mixed $id
     * @return Builder
     */
    protected function getModelsById($id, $column = null)
    {


        if (empty($column) || $column == null)
            $column = 'id';
        /**
         * If Eloquent Collection
         */
        if ($id instanceof Collection) {
            return $id;
        }
        /**
         * Raw #s/Array
         */


        $ids = (is_array($id)) ? $id : (!preg_match('#^[1-9][0-9]*(,[1-9][0-9]*)*$#', $id) ? [$id] : (explode(',', $id)));
        /**
         * Return Builder
         */

        $response = null;
        if (!empty($ids)) {
            $response = call_user_func_array([ $this->model(true), 'whereIn'], [$column, $ids]);
            return $response->with($this->relations)->get();
        } else {
            return $response->get();
        }
    }




    public function get($id, $column = null)
    {
        if (empty($column) || $column == null)
            $column = 'id';
        /**
         * If Eloquent Collection
         */
        if ($id instanceof Collection) {
            return $id;
        }
        /**
         * Raw #s/Array
         */
        $ids = (is_array($id)) ? $id : [$id];
        /**
         * Return Builder
         */

        $this->selected = call_user_func_array([ $this->model(true), 'where'], [$column, $ids]);
        if (!empty($ids)) {
            $response = call_user_func_array([ $this->model(true), 'whereIn'], [$column, $ids]);
            return $this->selected->get();
        } else {
            return $this->selected->get();
        }
    }

    /**
     * Register a Value Mutator
     * @param String $property
     * @param callable $mutate
     */
    public function addMutator($property, callable $mutate)
    {
        if (is_string($property)) {
            $this->mutators[$property] = $mutate;
        }
    }

    /**
     * Get Final Data Output
     * @return array
     */
    public function getData()
    {
        $out = [];
        $data = (!empty($this->data)) ? $this->data : $this->request->all();
        foreach ($data as $property => $value) {
            /** Mutate? */
            if (isset($this->mutators[$property]) && is_callable($this->mutators[$property])) {
                $value = call_user_func($this->mutators[$property], $value);
            }
            $out[$property] = $value;
        }
        return $out;
    }

    /**
     * @param string $lookUpValue
     * @param string|null $lookUpType
     * @return object
     */
    public function getLookUpObject(string  $lookUpValue, string $lookUpType=null) : object
    {
        $lookUpValue = strtoupper($lookUpValue);
        $lookupCollection = app('lookupContainer');
        $lookupObject = $lookupCollection[$lookUpValue];
        return $lookupObject;
    }

}


