<?php

namespace App\Policies;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class LoanPolicy
 * @package App\Policies
 */
class LoanPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user){
        return $user->can('view all loans');
    }

    /**
     * @param User $user
     * @param Loan $loan
     * @return bool
     */
    public function view(User $user, Builder $loan){
        if ($user->can('view all loans')) {
            return true;
        }

        return $user->id == $loan->user_id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user){
        return  false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user){
        // admin overrides user scopes
        if ($user->can('edit all loans')) {
            return  true;
        }
        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user)
    {
        // admin overrides user scopes
        if ($user->can('delete all loan')) {
            return  true;
        }

        return false;
    }

    /*TODO : other operational policies can be added*/
}
