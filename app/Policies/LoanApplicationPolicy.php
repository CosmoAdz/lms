<?php

namespace App\Policies;

use App\Models\LoanApplication;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any loan applications.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($user->can('view all loan applications')) {
            return true;
        }
        return  false;
    }

    /**
     * Determine whether the user can view the loan application.
     *
     * @param \App\Models\User|null $user
     * @param \App\Models\LoanApplication $loanApplication
     * @return mixed
     */
    public function view(?User $user, LoanApplication $loanApplication)
    {
        // admin overrides user scopes
        if ($user->can('view all loan applications')) {
            return true;
        }

        // users can view their loan applications
        return $user->id == $loanApplication->user_id;
    }

    /**
     * Determine whether the user can create loan applications.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create loan applications')) {
            /* more admin rules*/
            return true;
        }

        if ($user->can('create user loan applications')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the loan applications.
     *
     * @param \App\User $user
     * @param \App\Models\LoanApplication $loanApplications
     * @return mixed
     */
    public function update(User $user, LoanApplication $loanApplications)
    {
        // admin overrides user scopes
        if ($user->can('edit all loan applications') && $loanApplications->loan_status_text != 'approved') {
            return  true;
        }

        if ($user->can('edit own loan applications' && $loanApplications->loan_status_text != 'approved')) {
            return $user->id == $loanApplications->user_id;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the loan applications.
     *
     * @param \App\Models\User $user
     * @param \App\Models\LoanApplication $loanApplication
     * @return mixed
     */
    public function delete(User $user, LoanApplication $loanApplication)
    {
        // admin overrides user scopes
        if ($user->can('delete all loan applications')) {
            return  true;
        }

        if ($user->can('delete own loan applications')) {
            return $user->id == $loanApplication->user_id;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the loan applications.
     *
     * @param \App\Models\User $user
     * @param \App\Models\LoanApplication $loanApplication
     * @return mixed
     */
    public function restore(User $user, LoanApplication $loanApplication)
    {
        // admin overrides user scopes
        if ($user->can('restore all loan applications')) {
            return  true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the loan applications.
     *
     * @param \App\Models\User $user
     * @param \App\Models\LoanApplication $loanApplication
     * @return mixed
     */
    public function forceDelete(User $user, LoanApplication $loanApplication)
    {
        // admin overrides user scopes
        if ($user->can('force delete all loan applications')) {
            return  true;
        }
        return false;
    }
}