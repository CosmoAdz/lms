<?php
namespace App\Policies;

use App\Models\Loan;
use App\Models\LoanPayment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPaymentPolicy{
    use HandlesAuthorization;


    public function viewAny(User $user){
        return $user->can('view all loan payments');
    }

    public function view(User $user, LoanPayment $loanPayment){
        if ($user->can('view all loan payments')) {
            return true;
        }

        return $user->id == $loanPayment->user_id;
    }

    public function create(User $user){
        if($user->can('create user loan payments'))
        {
            return true;
        }

        if($user->can('create loan payments'))
            return  true;

        return  false;

    }

    public function update(User $user){
        // admin overrides user scopes
        if ($user->can('edit all loan payments')) {
            return  true;
        }
        return false;
    }

    public function delete(User $user)
    {
        return false;
    }

    /*TODO : other operational policies can be added*/

}
