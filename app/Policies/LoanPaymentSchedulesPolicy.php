<?php

namespace App\Policies;

use App\Models\LoanPaymentSchedule;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPaymentSchedulesPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->can('view all payment schedules');
    }

    public function view(User $user, LoanPaymentSchedule $schedule)
    {
        if ($user->can('view all loan schedules')) {
            return true;
        }

        return $user->id == $schedule->user_id;
    }

    public function create(User $user)
    {
        return $user->can('create loan schedules');
    }


    public function update(User $user)
    {
        // admin overrides user scopes
        if ($user->can('edit all loan schedules')) {
            return true;
        }
        return false;
    }

    public function delete(User $user)
    {
        return false;
    }

    /*TODO : other operational policies can be added*/

}
