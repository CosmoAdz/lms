<?php namespace App\Providers;


use App\Events\LoanApplicationObserver;
use App\Http\Requests\LoanApplicationRequest;
use App\Models\LoanApplication;
use App\Repositories\LoanApplicationRepository;
use Illuminate\Support\ServiceProvider;
use Predis\Client;


/**
 * Model Observer Provider
 *
 * -- Updates Related Eloquent Models on (CRUD) Operations
 */
class ObserverServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap Services
     * @return void
     */
    function boot()
    {
        LoanApplication::observe(app()->make('App\Events\LoanApplicationObserver'));

    }

    /**
     * Register Events
     * @return void
     */
    function register()
    {

        /**
         * Advertiser Observer
         */
        $this->app->bind('App\Events\LoanApplicationObserver', function ($app) {
            return new LoanApplicationObserver($this->app[LoanApplicationRequest::class], $this->app[LoanApplicationRepository::class]);
        });


    }

}

