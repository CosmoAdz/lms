<?php

namespace App\Providers;

use App\Models\Loan;
use App\Models\LoanApplication;
use App\Models\LoanPayment;
use App\Models\LoanPaymentSchedule;
use App\Policies\LoanApplicationPolicy;
use App\Policies\LoanPaymentPolicy;
use App\Policies\LoanPaymentSchedulesPolicy;
use App\Policies\LoanPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        /*define policy ; since it's lumen there is no provision for policy array*/
        Gate::policy(LoanApplication::class, LoanApplicationPolicy::class);
        Gate::policy(Loan::class, LoanPolicy::class);
        Gate::policy(LoanPaymentSchedule::class, LoanPaymentSchedulesPolicy::class);
        Gate::policy(LoanPayment::class, LoanPaymentPolicy::class);

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
    }
}
