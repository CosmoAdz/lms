<?php

namespace App\Providers;


use App\Http\Controllers\API\LoanApplicationController;
use App\Http\Controllers\API\LoanController;
use App\Http\Controllers\API\LoanPaymentController;
use App\Http\Controllers\API\LoanPaymentScheduleController;
use App\Http\Controllers\API\UserController;
use App\Http\Requests\LoanApplicationRequest;
use App\Http\Requests\LoanPaymentRequest;
use App\Http\Requests\LoanPaymentScheduleRequest;
use App\Http\Requests\LoanRequest;
use App\Http\Requests\UserLoginRequest;
use App\Repositories\LoanApplicationRepository;
use App\Repositories\LoanPaymentRepository;
use App\Repositories\LoanPaymentScheduleRepository;
use App\Repositories\LoanRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Tymon\JWTAuth\JWTAuth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserRepository::class, function ($app) {
            return new UserRepository($app[UserLoginRequest::class]);
        });

        $this->app->bind(UserController::class, function ($app) {
            return new UserController($app[UserLoginRequest::class], $app[JWTAuth::class], $app[UserRepository::class]);
        });


        $this->app->singleton(LoanApplicationRepository::class, function ($app) {
            return new LoanApplicationRepository($app[LoanApplicationRequest::class]);
        });

        $this->app->bind(LoanApplicationController::class, function ($app) {
            return new LoanApplicationController($app[LoanApplicationRequest::class], $app[JWTAuth::class], $app[LoanApplicationRepository::class]);
        });

        $this->app->singleton(LoanRepository::class, function ($app) {
            return new LoanRepository($app[LoanRequest::class]);
        });

        $this->app->bind(LoanController::class, function ($app) {
            return new LoanController($app[LoanRequest::class], $app[JWTAuth::class], $app[LoanRepository::class]);
        });

        $this->app->singleton(LoanPaymentScheduleRepository::class, function ($app) {
            return new LoanPaymentScheduleRepository($app[LoanPaymentScheduleRequest::class]);
        });

        $this->app->bind(LoanPaymentScheduleController::class, function ($app) {
            return new LoanPaymentScheduleController($app[LoanPaymentScheduleRequest::class], $app[JWTAuth::class], $app[LoanPaymentScheduleRepository::class]);
        });

        $this->app->singleton(LoanPaymentRepository::class, function ($app) {
            return new LoanPaymentRepository($app[LoanPaymentRequest::class]);
        });

        $this->app->bind(LoanPaymentController::class, function ($app) {
            return new LoanPaymentController($app[LoanPaymentRequest::class], $app[JWTAuth::class], $app[LoanPaymentRepository::class]);
        });

    }
}
