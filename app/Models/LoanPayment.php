<?php
namespace App\Models;

class LoanPayment extends Model{
    protected $table = 'loan_payments';
    /*to support manual updates for these fields*/
    protected $fillable = ['amount_paid', 'surplus_amount','late_charges','payment_mode','balance'];
    protected $guarded = ['loan_id','user_id','payment_cycle','payment_date'];

    protected $appends = [];


    public function loan(){
        return $this->belongsTo(Loan::class);
    }

}
