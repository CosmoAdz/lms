<?php
namespace App\Models;

class LoanApplication extends Model{
    protected $table = 'loan_applications';
    protected $fillable = ['loan_amount', 'description', 'loan_tenure','tenure_type_text', 'loan_type_text', 'loan_type_id', 'user_id', 'loan_status_text', 'loan_status_id'];
    protected $guarded = ['loan_status_text','loan_type_text'];

    protected $appends = [];
}
