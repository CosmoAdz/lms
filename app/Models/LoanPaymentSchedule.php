<?php
namespace App\Models;

class LoanPaymentSchedule extends Model{
    protected $table = 'loan_payment_schedules';
    /*to support manual updates for these fields*/
    protected $fillable = ['paid_on', 'is_pending'];
    protected $guarded = ['loan_id','user_id','payment_cycle','payment_date','next_payment_date'];

    protected $appends = [];
}
