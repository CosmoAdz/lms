<?php
namespace App\Models;

class Loan extends Model{
    protected $table = 'loans';
    protected $fillable = ['loan_application_id', 'user_id','sanctioned_amount', 'interest_rate', 'total_payment_cycles', 'tenure_numeral',
                           'tenure_type_id','is_net_off_allowed','is_defaulting','pending_payment_cycles'];
    protected $guarded = ['tenure_type_text'];


    protected $appends = [];


    public function schedules(){
        return $this->hasMany(LoanPaymentSchedule::class);
    }
}
