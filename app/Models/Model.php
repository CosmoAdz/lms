<?php

namespace App\Models;
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 14/4/17
 * Time: 2:30 PM
 */

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\MySqlConnection as mysql;
use Carbon\Carbon;
use JsonSerializable;

class Model extends Eloquent implements JsonSerializable
{
    protected $db;
    protected $reporting;
    public static $withoutAppends = false;
    protected array $relationMap;
    protected function getArrayableAppends()
    {
        if (self::$withoutAppends) {
            return [];
        }
        return parent::getArrayableAppends();
    }

    /**
     * Mutator: `created_at`
     * @param Carbon $date
     * @return String
     */
    public function getCreatedAtAttribute($date)
    {

        return Carbon::parse($date)->format('Y-m-d');//->timestamp * 1000;
    }

    /**
     * Mutator: `updated_at`
     * @param Carbon $date
     * @return String
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');//->timestamp * 1000;
    }
    /**
     * Get Reporting API
     * @return Monetize\Reporting\API
     */

    /**
     * Get DB
     * @return mysql
     */
    public function db()
    {
        return app()->make('db');
    }

    /**
     * Get User
     * @return [type] [description]
     */
    public function getUser()
    {
        return app()->make('auth')->user();
    }

    /**
     * Get Fillable Keys for Current Model
     * @return Array
     */
    public function getKeysAttribute()
    {
        return $this->fillable;
    }

    protected function getAttributeFromLookup(int $lookup_id)
    {
        $lookupContainer = app('lookupContainer');
        $status = '';
        array_walk($lookupContainer, function ($lookupObject) use ($lookup_id, &$status) {
            if ($lookupObject->getId() == $lookup_id) {
                $status = $lookupObject->getName();
            }

        });
        /*TODO : to be changed and moved to response traits.*/
        return strtolower($status);

    }


}
