<?php namespace App\Events;

use App\Models\LoanApplication;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


/**
 *
 */
class LoanApplicationObserver extends Observer
{

    public function creating(LoanApplication $loanApplication)
    {
        $loanApplication->user_id = auth()->id();
    }

    public function saved(LoanApplication $loanApplication)
    {
        try {
            if ($loanApplication->loan_status_text == 'approved') {
                /*Create Loan*/


                $interestRate = $this->repository->getInterestRate();

                $repayment_amount = $this->repository->getRepaymentAmount($loanApplication->loan_amount, $loanApplication->loan_tenure, $interestRate);
                $total_payment_cycles = $this->repository->getTotalPaymentCycles($loanApplication->loan_amount, $repayment_amount);
                $lateCharges = $this->repository->getLateCharges($loanApplication->loan_amount);
                DB::beginTransaction();
                $loanId = DB::table('loans')->insertGetId([
                    'loan_application_id'=>$loanApplication->id,
                    'user_id'=>$loanApplication->user_id,
                    'sanctioned_amount'=>$this->repository->getSanctionedAmount($loanApplication->loan_amount),
                    'interest_rate'=>$interestRate,
                    'total_payment_cycles'=>$total_payment_cycles,
                    'tenure_numeral'=>$loanApplication->loan_tenure
                ]);
                $loanPaymentSchedules = [];
                $nextPaymentDate = null;
                for ($i = 0; $i < $total_payment_cycles; $i++) {
                    $multiplier = $i * 7;
                    $date = Carbon::today()->addDays($multiplier)->toDateString();
                    if ($i < $total_payment_cycles)
                        $nextPaymentDate = Carbon::today()->addDays($multiplier + 7)->toDateString();
                    else
                        $nextPaymentDate = null;

                    $lastPaymentDate = Carbon::today()->addDays($multiplier + 2)->toDateString();

                    $loanPaymentSchedules[] = [
                        'loan_id' => $loanId,
                        'amount' => $repayment_amount,
                        'user_id' => $loanApplication->user_id,
                        'payment_cycle' => $i+1,
                        'payment_date' => $date,
                        'late_charges' => $lateCharges,
                        'next_payment_date' => $nextPaymentDate,
                        'last_payment_date' => $lastPaymentDate


                    ];
                }
                DB::table('loan_payment_schedules')->insert($loanPaymentSchedules);
                DB::commit();
            }

        } catch (\Exception $exception) {
            DB::rollback();
            $this->log('ALERT EXCEPTION', ['exception' => $exception], 'error');
            throw $exception;
        }

    }
}
