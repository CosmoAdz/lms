<?php
namespace App\Events;
use App\Http\Requests\BaseRequest;
use App\Repositories\Repository;
use App\Traits\LoggerTrait;
use Predis\Client;

abstract class Observer {
    use LoggerTrait;
    /**
     * @var Repository
     */
    protected Repository $repository;
    /**
     * @var BaseRequest
     */
    protected BaseRequest $request;
    /**
     * @var false
     */
    protected bool $isLive;
    /**
     * @var Client
     */
    protected Client $redis;

    /**
     * Construct Observer
     * @param BaseRequest $request
     * @param Repository $repository
     */
    public function __construct(BaseRequest $request, Repository $repository) {
        $this->request    = $request;
        $this->repository = $repository;
        /*TODO : load from env.*/
        $this->isLive = false;
    }

}