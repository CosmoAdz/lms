<?php namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Repositories\Repository;
use \App\Http\Requests\BaseRequest;
class UserController extends ApiController
{
    protected  Repository $repository;
    protected  BaseRequest $request;


    public function register(){
        $this->request->applyValidation();

        $plainPassword = $this->request->input('password');
        $hash = app('hash')->make($plainPassword);

        try {
            /*call create on repo.*/

            $user = $this->repository->create([
                /*more fields can be added once schema is updated*/
                'user_name' => $this->request->input('name'),
                'user_email' => $this->request->input('email'),
                'password' => $hash,
                'user_phone' => $this->request->input('mobile'),
            ]);
            $user->assignRole('user');
            return $this->success($user);
        } catch (\Exception $exception) {
            return $this->error('Registration Failed ! please contact admin.');
        }
    }

    public function login(){
        $this->request->applyValidation();

        $credentials = $this->request->only(['user_email', 'password']);

        if (!$token = Auth::guard('api')->attempt($credentials)) {
            return $this->error('Unauthorized', true, 401);
        }

        return $this->success($this->prepToken($token));
    }

}