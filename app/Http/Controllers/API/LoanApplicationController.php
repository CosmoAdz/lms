<?php namespace App\Http\Controllers\API;

use App\Http\Requests\BaseRequest;
use App\Repositories\Repository;

class LoanApplicationController extends ApiController
{
    protected  Repository $repository;
    protected  BaseRequest $request;


    public function approve(int $loanApplicationId){
        try {

            $loanApplication  = $this->getModelsQueryBuilder($loanApplicationId);
            /*
             *
             * have considered only "update" permission
             * */
            $this->authorize('update',$loanApplication->getModels());

            $this->repository->update($loanApplicationId,[
                'loan_status_text'=>'approved'
            ]);

            return $this->success('Successfully updated !');
        }
        catch (\Exception $exception){
            $this->log('ALERT EXCEPTION', ['exception' => $exception], 'error');
            return $this->error($exception->getMessage(), false, 500);
        }

    }
}