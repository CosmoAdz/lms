<?php namespace App\Http\Controllers\API;

use App\Http\Requests\BaseRequest;
use App\Models\Loan;
use App\Models\LoanPaymentSchedule;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class LoanPaymentController extends ApiController
{
    protected Repository $repository;
    protected BaseRequest $request;


    public function repay(int $loanId)
    {

        try {

            /*get latest payment entry*/
            $latestPayment = $this->repository->findWhere(['loan_id' => $loanId])->orderBy('payment_date', 'desc')->latest()->get();
            /*check */
            $date = $this->request->input('date');
            if (is_null($date) || empty($date))
                $date = Carbon::today()->toDateString();
            $amount = $this->request->input('amount');
            $loan = Loan::where('id',$loanId)->first();
            $schedule = LoanPaymentSchedule::where('payment_date', '<=',$date)->orderBy('payment_date', 'desc')->latest()->first();
            $balance =$loan->sanctioned_amount-$amount;
            if ($latestPayment->isEmpty()) {
                $latestPayment = DB::table('loan_payments')->insert([
                    'loan_id' => $loan->id,
                    'amount_paid' => $amount,
                    'user_id' => $loan->user_id,
                    'balance' => $balance,
                    'payment_cycle' => $schedule->payment_cycle,
                    'payment_date'=>$date
                ]);
            } else {
                $latestPayment = $latestPayment->first();
                /*
                 * Inherent assumption that UI would show the payment options as per the schema, wherein, pending loans will be given preference or their payments get
                 * added on to a final amount = amount (principal + interest)  + late_charges

                 * Here assumptions is that paid amount will be simply checked off against the balance of the loan account.
                 * */

                $balance = $latestPayment->balance - $amount;
                DB::table('loan_payments')->insert([
                    'loan_id' => $loanId,
                    'amount_paid' => $amount,
                    'user_id' => $loan->user_id,
                    'balance' => $balance,
                    'payment_cycle' => $schedule->payment_cycle,
                    'payment_date'=>$date
                ]);
            }
            /*since there is no UI, such response can convey the state of the database.*/
           return $this->success([
                'status' => 'paid',
                'loan_id' => $loanId,
                'balance' => $balance,
            ]);
        } catch (\Exception $exception) {
            $this->log('Alert Exception', ['repaymentException' => $exception], 'error');
            return $this->error('payment failed, please contact the administrator !');
        }


    }
}