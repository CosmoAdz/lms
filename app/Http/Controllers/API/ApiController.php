<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Repositories\Repository;
use App\Traits\CrudTraits;
use App\Traits\LoggerTrait;
use App\Traits\ResponseTraits;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;

class ApiController extends Controller
{
    use LoggerTrait;
    use ResponseTraits;
    use CrudTraits;


    /**
     * @var BaseRequest
     */
    protected BaseRequest $request;
    /**
     * @var Repository
     */
    protected Repository $repository;
    /**
     * @var JWTAuth
     */
    protected JWTAuth $jwt;

    /*PUBLIC*/
    public $selected = [];

    public static $meta = ['relations', 'appends', 'page', 'perPage', 'admin', 'sort', 'sortDirection'];
    public static $stats = [];
    public static $relations = [];


    public function __construct( BaseRequest $request, JWTAuth $JWTAuth, Repository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->jwt = $JWTAuth;
    }


    /*
     * Action Handler
     * @param string | Integer $actionorId
     * @return Response   [description]
     * */
    public function handle($actionOrId = null)
    {

        try {
            if (method_exists($this, $actionOrId)) {
                return call_user_func([$this, $actionOrId]);
            }
            return static::crud($actionOrId);
        } catch (\Exception $exception) {
            $this->log('ALERT EXCEPTION', ['exception' => $exception], 'error');
            return $this->error($exception->getMessage(), false, 500);
        }
    }

    /**
     * Valid Object Identifier?
     * @param mixed $id
     * @return boolean
     */
    protected function isValidId($id)
    {
        return !empty($id) && (is_array($id) || intval($id) == $id);
    }
}
