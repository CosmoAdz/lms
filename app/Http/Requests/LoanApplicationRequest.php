<?php namespace App\Http\Requests;

class LoanApplicationRequest extends BaseRequest
{
    protected array $sanity = [];
    protected array $rules = ['new'=>[
        'loan_amount'=>'required',
        'description'=>'required',
        'loan_tenure'=>'required',
    ]

    ];
    protected array $messages = [
        'new'=>[
            'loan_amount'=>'this is a required parameter',
            'description'=>'this is a required parameter',
            'loan_tenure'=>'this is a required parameter',
    ]];

}
