<?php namespace App\Http\Requests;

class UserLoginRequest extends BaseRequest
{

    protected array $sanity = ['user_email', 'password'];
    protected array $rules = ['user_email' => 'required', 'password' => 'required'];
    protected array $messages = ['user_email' => 'email is a required field', 'password' => 'password is a required field'];

}
