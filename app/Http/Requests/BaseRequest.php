<?php namespace App\Http\Requests;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

abstract class BaseRequest extends Request
{
    /*
     * Request provides abstraction to define validation rules for each route type. I have kept the implementation very simple and straightforward.
     *
     * */
    use ProvidesConvenienceMethods;

    protected array $sanity = [];
    protected array $rules = [];
    protected array $messages = [];

    protected string $requestType = '';
    protected string $messageType = '';

    public function setRequestType( string $requestType){
        $this->requestType = $requestType;
    }

    public function setMessageType(string $messageType){
        $this->messageType = $messageType;
    }

    public function applyValidation()
    {
        if (!empty($this->requestType) && !empty($this->messageType))
            $this->validate($this, $this->rules[$this->requestType], $this->messages[$this->messageType]);
        else
            $this->validate($this, $this->rules, $this->messages);
    }


    /*sanitize inputs
    TODO : couldn't finish this, may be should have used Fractal-transformers.
    */
    public function sanitize()
    {
        $requestAttributes = $this->all();
        $sanitized_attributes = array_intersect_key($this->sanity, $requestAttributes);
        if ($sanitized_attributes) {
            $sanitized_attributes = array_combine(
                array_values($this->sanity), array_values($requestAttributes)
            );


            /*this fails to replace, it only replaces that exists.*/
            $this->replace($sanitized_attributes);
            return $this->all();
        }
        return [];

    }


}
