<?php namespace App\Http\Requests;

class LoanPaymentRequest extends BaseRequest
{
    protected array $sanity = [];
    protected array $rules = ['amount'=>'required'];
    protected array $messages = ['amount'=>'this is a required field'];

}
