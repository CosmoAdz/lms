<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWT;

class AuthMiddleware
{

    protected $except = ['api/user/login', 'api/user/register'];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->filterSpecialRoutes($request))
            return $next($request);

        $token = $request->bearerToken();
        if (!$token) {
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }

        $payload = null;
        try {
            $jwt = app()->make(JWT::class);
            $token = $jwt->getToken();
            $payload = $jwt->getPayload($token)->toArray();
        } catch (TokenExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 401);
        } catch (\Exception $e) {
            \Log::info('exception',['ex'=>$e]);
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 401);
        }
        // set data to user
        /*TODO : store user object*/
        return $next($request);
    }

    private function filterSpecialRoutes($request)
    {

        try {
            $skip = false;
            foreach ($this->except as $specialRoute) {
                if ($request->path() === $specialRoute) {
                    \Log::debug("Skipping $specialRoute from auth middleware check(s)");
                    $skip = true;
                }
            }
            return $skip;
        } catch (\Exception $exception) {
            \Log::debug("exception in routes", ['exc' => $exception]);
        }

    }


}
