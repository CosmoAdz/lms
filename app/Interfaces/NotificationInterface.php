<?php
namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model as Eloquent;

interface NotificationInterface {


    /**
     * Notification Handler
     * @param  string 	 $event
     * @param  Eloquent  $model
     * @param  array  	 $options
     * @return void
     */
    public function notify($event, Eloquent $model, array $options = []);

}

