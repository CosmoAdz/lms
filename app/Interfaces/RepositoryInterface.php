<?php namespace App\Interfaces;


use App\Http\Requests\BaseRequest;
use App\Models\Model;
use Illuminate\Http\Request;

interface RepositoryInterface
{
    /**
     * Constructor
     * @param Request $request
     *
     */
    public function __construct(BaseRequest $request);

    /**
     * Create/Load a New Model
     * @param  array $data
     * @return Model
     */
    public function create(Array $data = []);

    /**
     * List Models Accessible by User
     * @return [type] [description]
     */
    public function all();

    /**
     * Get by ID(s)
     * @param  mixed $id Array or Integer
     * @return mixed       Model | Array of Models
     */
    public function get($id);

    /**
     * Delete by ID(s)
     * @param  mixed $id Array or Integer
     * @return Boolean       true | false
     */
    public function delete($id);

    /**
     * Update by ID(s)
     * @param  mixed $id Array or Integer
     * @return mixed       Model | Array of Models
     */
    public function update($id, $data);
}
