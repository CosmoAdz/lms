<?php namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as SqlBuilder;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

trait CrudTraits
{
    /**
     * Create/Read/Update/Delete
     * @param string | Integer $actionOrId
     * @return Response
     */
    protected $method;
    protected $selected_count;
    protected $request_data;

    public function crud($actionOrId = null)
    {
        $this->request_data = $this->request->all();
        $filtered = array_merge(static::$meta, static::$relations);

        try {

            /**
             * CRUD Router
             */
            $count = count(array_except($this->request_data, $filtered));
            if ($this->request->isMethod('get')) {

                /**
                 * GET (Simple Read)
                 */
                $builder =$this->getModelsQueryBuilder(
                    $actionOrId
                );
                    $this->authorize('view',$builder->getModels());
                return $this->success(
                    $builder
                );

            } elseif ($this->request->isMethod('post')
                && ($actionOrId !== null || $this->request->has('ids'))) {

                /**
                 * POST Create/Update
                 */
                $this->request->setRequestType($actionOrId);
                $this->request->setMessageType($actionOrId);
                //validate the request.
                $this->request->applyValidation();

                $this->selected = $this->getModelsQueryBuilder($actionOrId);
                $this->isWrite = true;

                return ($actionOrId === 'new')
                    ? $this->create()
                    : $this->update();

            } elseif ($this->request->isMethod('delete')) {

                /**
                 * DELETE
                 */
                $this->selected = $this->getModelsQueryBuilder($actionOrId);

                return $this->delete($actionOrId);

            }
            # Default Error
            return $this->error("We're sorry, but we can't process this request.");
        } catch (ValidationException $validationException) {
            $this->log('ALERT EXCEPTION', ['validationException' => $validationException], 'notice');
            return $this->validationError($validationException->validator->errors());

        } catch (\Exception $e) {
            $this->log('ALERT EXCEPTION', ['exceptionCrud' => $e], 'error');

            return $this->error($e->getMessage());
        }
    }

    /**
     * Create Resource
     * @return Model
     */
    public function create()
    {
        $this->authorize('create', $this->repository->getMock());
        $collection = $this->selected->get();
        if ($collection instanceof Collection) {
            return $this->success($collection);
        } else {
            return $this->error('Unable to create record.');
        }
    }

    public function search()
    {
        $Models = $this->getModelsQueryBuilder();
        if (method_exists($Models, 'get')) {
            $search = $this->request->input('search');
            $filters = $this->request->input('filter');
            foreach ($filters as $filter)
                $Models->where($filter, 'Like', '%' . $search . '%');

            return $this->success($Models->get());
        } else {
            return $this->error('Could not locate resources(s).search');
        }
    }

    /**
     * Retrieve Resources
     * @param bool $returnCollection
     * @return array
     */
    public function all()
    {

        $Models = $this->getModelsQueryBuilder();
        $this->authorize('viewAny',$Models);
        $collection = null;
        if ($this->request->has('conditions')) {
            $conditions = $this->request->input('conditions');
            if (!is_null($conditions) & !empty($conditions))
                $Models = $Models->where($conditions);
        } else
            $collection = $Models->get();
        $this->selected_count = $Models->count();


        return $this->success($collection);
    }



    /**
     * Retrieve Single Resource by ID
     * @param mixed $id
     * @return mixed
     */
    public function get($id)
    {
        $this->authorize('view',$this->repository->getMock());
        $Models = $this->getModelsQueryBuilder($id);
        if (method_exists($Models, 'get')) {
            return $this->success($Models->get());
        } else {
            return $this->error('Could not locate resource(s).get');
        }
    }


    /**
     * Update Resource by ID
     * @param mixed $id
     * @return mixed
     */
    public function update()
    {
        /**
         * Update via Repository
         */
        $Models = $this->repository->update($this->selected->get(), $this->request->all());
        /**
         * Records Updated?
         */
        if ($Models->count() > 0) {
            return $this->success($Models);
        } else {
            return $this->error('Could not locate resource(s).update');
        }
    }

    /**
     * Delete
     * @param mixed $actionOrId
     * @return Array
     */
    public function delete($actionOrId)
    {
        # Valid Eloquent Selection
        if ($actionOrId && $this->selected->count() > 0) {
            /**
             * Get Eloquent Collection from Builder
             */
            $collection = ($this->selected instanceof Collection)
                ? $this->selected
                : $this->selected->get();

            $collection->each(function ($Object) {
                $Object->delete();
            });
            # Return Success
            return $this->success($collection->pluck('id'));
        } else {
            return $this->error('Could not delete one or more ' . $this->repository->model() . '(s).');
        }
    }

    /**
     * Get Query Builder
     * @param string | Integer $actionOrId
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function getModelsQueryBuilder($actionOrId = null, $withCount = false)
    {
        /** Get Builder */
        switch (true) {
            case ($actionOrId === 'new'):
                $Model = $this->repository->create($this->request_data);

                /** Create Model */
                $Builder = call_user_func_array([$this->repository->model(true), 'where'], [$this->repository->table() . '.id', $Model->id]);
                break;
            case ($this->isValidId($actionOrId)):
                $Builder = $this->modelsFromRequest($actionOrId, true);

                break;
            default:
                /** All */
                $Builder = call_user_func([$this->repository->model(true), 'query']);
                break;
        }
        if ($withCount) {
            /** Set Count */
            $Counter = $Builder;
            $this->selected_count = $Counter->count();

        }


        //return $Builder;
        $this->setSort($Builder);

        return $this->paginate($Builder);
    }

    /**
     * Set Sorting
     * @param Builder $Builder
     * @param boolean $hasSort
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function setSort(Builder $Builder, $hasSort = false)
    {

        if ($this->request->has('sort')) {
            $sort = $this->request->input('sort');
            $appendMap = $this->repository->getMock()->relationMap;
            if (array_key_exists($sort, $appendMap)) {
                $sort = $appendMap[$sort];
            }

            $sortDirection = $this->request->input('sortDirection');
            switch ($sortDirection) {
                case 'asc':
                    $Builder->orderBy($sort);
                    break;
                case 'desc':
                    $Builder->orderBy($sort, $sortDirection);
                    break;
                default:
                    break;
            }
        }
        return $Builder;
    }



    /**
     * Get Models from Request
     * @param string | Integer $actionOrId
     * @param boolean $builder
     * @return Array | Illuminate\Database\Eloquent\Builder
     */
    public function modelsFromRequest($actionOrId, $builder = false)
    {
        # Table
        $table = $this->repository->table();
        # Get IDs
        $ids = ($this->isValidId($actionOrId)) ? [$actionOrId] : [];
        $ids = (!empty($this->request->input('ids'))) ? array_merge($this->request->input('ids'), $ids) : $ids;
        # Force Type
        array_walk($ids, 'intval');
        return $builder
            ? $this->repository->selected = call_user_func_array([$this->repository->model(true), 'whereIn'], [$table . '.id', $ids])
            : $this->repository->get($ids);
    }

    /**
     * Paginate Data
     * @return Array
     */
    public function paginate($data = null)
    {
        $page = $this->request->input('page', 0);
        $per = $this->request->input('perPage', 50);
        if ($data instanceof Builder || $data instanceof SqlBuilder) {
            /** DB Builder Pagination */
            $data->forPage($page + 1, $per);
        } elseif ($data instanceof Collection) {
            /** Model Collection Pagination */
            $data->forPage($page + 1, $per);
        } elseif (is_array($data)) {
            /** Array Pagination */
            if ($page > -1 && $per > 0 && !empty($data)) {
                array_values(array_slice($data, $page * $per, $per, true));
            }
        }


        return $data;
    }

    /**
     * get Appends for Collection
     * @param Collection $collection
     * @param Array $appends
     * @return Array
     */
    public function getAppendsForCollection($collection, $appends = [])
    {
        /**
         * Get Collection
         */
        $collection = ($collection instanceof Builder || $collection instanceof SqlBuilder)
            ? $collection->get()
            : $collection;
        /**
         * Get Appended Attributes
         */
        $appends = (is_array($appends))
            ? $appends
            : [];
        /**
         * Iterate valid...
         */
        if ($collection instanceof Collection) {
            /** With Properties */
            $data = [];
            $collection->each(function ($Object) use (&$data, $appends) {
                /** Recursively Appended */
                $data[] = $this->getObjectWithAppends($Object, $appends);
            });
            return $data;
        } else {
            return $collection;
        }
    }

    /**
     * Get Object With Appends (recursion-safe)
     * @param object $Object
     * @param Array $appends
     * @return Array
     */
    public function getObjectWithAppends($Object, array $appends = [])
    {
        $self = $this;
        $base = $Object->toArray();
        foreach ($appends as $column) {
            /**
             * Get Appended Objects
             */
            $base[$column] = $Object->{$column};
            /**
             * Iterate for N-Child Objects
             */
            while ($base[$column] instanceof Collection) {
                $objects = [];
                $base[$column]->each(function ($model) use ($self, $appends, &$objects) {
                    $objects[] = $self->getObjectWithAppends($model, $appends);
                });
                $base[$column] = $objects;
            }
        }
        /*To Remove Null From Response*/
        if ($this->request->has('raw') && !is_null($this->request->input('raw')))
            return array_filter($base);
        else
            return $base;
    }
}