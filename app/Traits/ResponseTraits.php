<?php namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

trait  ResponseTraits
{

    protected function prepToken($token)
    {
        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ];
    }

    /**
     * Error Response
     * @param mixed $message
     * @param string $redirect
     * @param integer $code
     * @return Response
     */
    public function error($message, $redirect = false, $code = 403)
    {
        try {
            return response()->json([
                'error' => $message,
                'data' => $message,
                'status' => 'error',
                'redirect' => $this->getRedirectAbsoluteUrl($redirect),
                'code' => $code
            ], $code);
        } catch (\Exception $exception) {

        }

    }

    public function validationError(MessageBag $errors)
    {
        try {
            $errors = $errors->messages();
            $validationError = [];
            array_walk($errors, function ($value, $key) use (&$validationError) {
                if (count($value) == 1)
                    $validationError[$key] = implode($value);
                else
                    $validationError[$key] = $value;
            });
            return response()->json(["code" => 406, "message" => "forbidden", "errors" => $validationError]);
        } catch (\Exception $exception) {

        }
    }

    /**
     * Success Response
     * @param mixed $data
     * @param string $redirect
     * @return Response
     */
    public function success($data, $redirect = false)
    {
        try {
            # With Appends
            if (method_exists($this, 'getAppendsForCollection')) {
                $data = $this->getAppendsForCollection($data, $this->request->input('appends', []));
            }

            # Pagination
            $page = $this->request->input('page', 0);
            $per = $this->request->input('perPage', 50);

            # Response
            $response = [
                'data' => $this->filter($data),
                'status' => 'success'
            ];

            # Counter?
            if (isset($this->selected_count)) {
                $response['total'] = $this->selected_count;
                $response['per_page'] = $per;
                $response['page'] = $page;
                $response['pages'] = ceil($this->selected_count / $per);
            }
            return response()->json($response);
        } catch (\Exception $e) {
            $this->log('ALERT EXCEPTION', ['exception' => $e], 'error');
            return $this->error('We are sorry, but we could not serve this request.!');
        }
    }

    /**
     * Filter API Response Data
     * @param mixed $data [description]
     * @return [type]       [description]
     */
    public function filter($data)
    {
        /**
         * Collection?
         * stdObject?
         */
        if (is_object($data)) {
            $data = (is_callable([$data, 'toArray'], true)) ? $data->toArray() : (array)$data;
        }

        if (is_array($data)) {
            foreach ($data as $i => $record) {
                if (is_object($record)) {
                    if (is_callable($record, 'toArray')) {
                        $record = $record->toArray();
                    } else {
                        $record = (array)$record;
                    }
                }
                /*  if (is_array($record)) {
                      $data[$i] = $this->authApi->filter($record, $this->isWrite);
                  }*/

            }
        }
        return $data;
    }


    /**
     * Get Redirect Full URL
     * @param string $redirect
     * @param Array $data
     * @return string
     */
    protected function getRedirectAbsoluteUrl($redirect, $data = [])
    {
        if ($redirect) {
            return (\App::make('Illuminate\Routing\Router')->has($redirect)) ? route($redirect, $data) : url($redirect);
        }
        return false;
    }
}
