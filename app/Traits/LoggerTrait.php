<?php namespace App\Traits;

use Illuminate\Support\Facades\Log;

trait LoggerTrait
{

    public function log(string $logIdentifier, array $logData, string $level = 'info'): void
    {
        //log if debug is set true at
        switch ($level) {
            case 'info' :
                //log if debug is enabled
                if (config('envConfig.env.APP_DEBUG')) {
                    Log::info($logIdentifier, $logData);
                }
                break;
            case 'debug' :
                if (config('envConfig.env.APP_DEBUG')) {
                    Log::debug($logIdentifier, $logData);
                }
                break;
            case 'alert' :
                Log::alert($logIdentifier, $logData);
                break;
            case 'warning' :
                Log::warning($logIdentifier, $logData);
                break;
            case 'notice':
                Log::notice($logIdentifier, $logData);
                break;
            case 'emergency' :
                Log::emergency($logIdentifier, $logData);
                break;
            case 'critical' :
                Log::critical($logIdentifier, $logData);
                break;
            case 'error' :
                Log::error($logIdentifier, $logData);
                break;

        }

    }
}
