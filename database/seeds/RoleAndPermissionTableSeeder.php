<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsTableSeeder extends Seeder
{

    public function run()
    {

        /*admin permissions & roles*/
        $adminOpKeywords = ['user', 'loans', 'loan applications', 'loan payments', 'loan schedules'];
        $userOpKeywords = ['loan applications', 'loans'];

        $adminPermissionContexts = [
            'create ', 'view all ', 'edit all ', 'delete all '];

        $userPermissionContexts = ['create user ', 'edit own '];

        $permissions = [];
        // have kep simple loops and implementation since its one time run.
        /*
         * create admin role.
         *
         * */
        foreach ($adminPermissionContexts as $context) {

            foreach ($adminOpKeywords as $keyword) {

                $permissions[] = ['name' => $context . $keyword, 'guard_name' => 'api', 'permission_type' => 'admin'];

            }
        }
        DB::table('permissions')->insert($permissions);
        $permissions = [];
        /*
         *
         *create user role*/

        foreach ($userPermissionContexts as $context) {

            foreach ($userOpKeywords as $keyword) {

                $permissions[] = ['name' => $context . $keyword, 'guard_name' => 'api', 'permission_type' => 'user'];

            }
        }
        DB::table('permissions')->insert($permissions);


        $adminRole = Role::create(['name' => 'admin']);
        $userRole = Role::create(['name' => 'user']);
        $adminPermissionsCollection = Permission::where('permission_type', 'admin')->pluck('id', 'id')->all();
        $userPermissionsCollection = Permission::where('permission_type', 'user')->pluck('id', 'id')->all();
        $adminRole->syncPermissions($adminPermissionsCollection);
        $userRole->syncPermissions($userPermissionsCollection);

    }


}