<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $users = [
            //admin
            'admin' => [
                'is_user_admin' => true,
                'user_email' => 'admin@aspire.com',
                'password' => app('hash')->make('admin'),
                'user_name' => 'admin',
                'user_phone' => '9876543210',
            ]
            ,
            //user
            'user'=>['is_user_admin' => false,
                'user_email' => 'user@aspire.com',
                'password' => app('hash')->make('user'),
                'user_name' => 'user',

                'user_phone' => '9876543210',

                'is_salaried'=>true,
                'total_yearly_income'=>'1000000',
                'user_category_text'=>'med',
            ]

        ];

        foreach ($users as $userType => $user){
           $user= User::create($user);
           $role = \Spatie\Permission\Models\Role::where('name',$userType)->first();
           \Illuminate\Support\Facades\DB::table('model_has_roles')->insert([
              'role_id'=>$role->id,
               'model_type'=>'App\Models\User',
               'model_id'=>$user->id
           ]);
        }
    }


}