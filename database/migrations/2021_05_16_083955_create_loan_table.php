<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*each loan is to be considered as a loan account for a user, so single user can have many loans (ideally)*/
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_application_id');
            $table->foreign('loan_application_id')
                ->references('id')
                ->on('loan_applications');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->decimal('sanctioned_amount',15, 2);
            $table->decimal('interest_rate');

            $table->integer('total_payment_cycles');
            $table->integer('tenure_numeral');

            $table->integer('remaining_cycles')->nullable();
            /*this field defines the rule to settle surplus payments, I have considered simply net-off scenario where in extra paid amount will be deducted from
            balance amount, however, it can stored in a separate table as loan_rules/loan_policies where rules can be stored for such cases.*/
            $table->boolean('is_net_off_allowed')->default(true);
            $table->string('tenure_type_text')->default('month');
            $table->unsignedBigInteger('tenure_type_id')->nullable();
            $table->foreign('tenure_type_id')
                ->references('id')
                ->on('lookup');

            $table->boolean('is_defaulting')->nullable();
            /* each number signifies missed payments for each cycle.*/
            $table->integer('pending_payment_cycles')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan');
    }
}
