<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*a user can apply for n number of loan applications (ideally)*/
        Schema::create('loan_applications', function (Blueprint $table) {
            $table->id();
            /*have considered only two primary fields for loan application*/
            $table->decimal('loan_amount', 15, 2);
            $table->integer('loan_tenure');

            $table->string('tenure_type_text')->default('month');
            $table->unsignedBigInteger('tenure_type_id')->nullable();
            $table->foreign('tenure_type_id')
                ->references('id')
                ->on('lookup');

            $table->longText('description')->nullable();
            /*have added an extra field, to give more meaning to this use case*/
            $table->string('loan_type_text')->default('personal');
            $table->unsignedBigInteger('loan_type_id')->nullable();
            $table->foreign('loan_type_id')
                ->references('id')
                ->on('lookup');

            /*user bound details*/
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            /*user follow up (?) can be separated into a separate table, if updates become expensive over time.*/
            $table->string('loan_status_text')->default('draft'); // can be 'approved' or 'declined'
            $table->unsignedBigInteger('loan_status_id')->nullable();
            $table->foreign('loan_status_id')
                ->references('id')
                ->on('lookup');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_application_tables');
    }
}
