<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            /*
             * Decided to user admin scope within the same table to keep this implementation simple and within the scope of the "Test"
             *
             * */
            $table->boolean('is_user_admin')->default(false);
            $table->string('user_email');
            $table->string('password');
            $table->string('user_name');
            $table->string('user_phone');
            /*if user defaults on any of the loans or based on other business logic, the user can be flagged for the future.*/
            $table->boolean('is_defaulter')->default(false);
            $table->boolean('is_salaried')->default(false);
            /*income can be farm, ancestral or rents, can be separated into a separate table later as user_incomes*/
            $table->text('income_details')->nullable();
            $table->decimal('total_yearly_income',15, 2)->nullable();
            $table->string('user_category_text')->nullable();
            /*should be part of lookup table : can be ['low','medium','high'] ; have taken some liberties here.*/

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
