<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanPaymentSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_payment_schedules', function (Blueprint $table) {

            /*this table will have pre-fed entries as per the "loan total_payment_cycles value ", for example
            if for loan_id = 1, total_payment_cycles = 3, then there will be three entries in this table for every date
            the payment is supposed to be paid. The same table can also be used to schedule routine sms/mails to the user.
            */
            $table->id();
            /*for which loan account*/
            $table->unsignedBigInteger('loan_id');
            $table->foreign('loan_id')
                ->references('id')
                ->on('loans');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            /*have considered only "weekly payments",
            though this column can be broken into amount and payment_cycle_type : weekly monthly, yearly, quarterly etc
            */
            $table->decimal('amount',15, 2);
            $table->decimal('late_charges')->nullable();
            /*cycle count*/
            $table->integer('payment_cycle');
            /*date for this payment ; to be paid when ?*/
            $table->date('payment_date');
            /*applicable date beyond which there will be extra charges.*/
            $table->date('last_payment_date');
            /*look ahead for next cycle*/
            $table->date('next_payment_date')->nullable();

            /*follow up details*/
            $table->date('paid_on')->nullable();

            $table->boolean('is_pending')->default(true);
            $table->softDeletes();
            $table->timestamps();  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_payment_schedules');
    }
}
