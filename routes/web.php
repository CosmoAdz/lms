<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Aspire Test Project";
});


$router->group(['prefix' => 'api'], function () use ($router) {

    /*TODO : more routes can be added as required, all models have CRUD setup*/

    // Matches "/api/register
    $router->post('user/register', 'UserController@register');
    // Matches "/api/login
    $router->post('user/login', 'UserController@login');


    $router->get('loanapplication/{actionOrId}','LoanApplicationController@handle');
    $router->post('loanapplication/{actionOrId}','LoanApplicationController@handle');

    $router->post('loanapplication/{loanApplicationId}/approve','LoanApplicationController@approve');


    $router->get('loans/{actionOrId}','LoanController@handle');
    $router->put('loans/{actionOrId}','LoanController@handle');

    $router->get('schedules/{actionOrId}','LoanPaymentScheduleController@handle');
    $router->put('schedules/{actionOrId}','LoanPaymentScheduleController@handle');

    $router->get('loans/{actionOrId}','LoanController@handle');
    $router->put('loans/{actionOrId}','LoanController@handle');

    $router->get('loanpayment/{actionOrId}','LoanPaymentController@handle');
    $router->put('loanpayment/{actionOrId}','LoanPaymentController@handle');

    $router->post('loanpayment/{loanId}/repay','LoanPaymentController@repay');



});
